# Ansible simple hardening and Wazuh Agent Installation on windows

This playbook is used for disabling NetBios (via registry editor and powershell) and then install and register wazuh agent to the wazuh manager. Installation files is gathered by downloading the agent to certain URL (for this sample it will use official wazuh agent url installation files on windows).

to start the playbook make sure edit the `host.ini` file to change the IP to your windows server IP to be hardened and regist the wazuh agent
```
windows-machine ansible_host=192.168.56.10
```

Second, make sure that you changed the `wazuh_agent_url` and `wazuh_manager_ip` in the files `vars.yml` to the correct URL and wazuh manager's IP on your environment

Last, create a files called `credentials.yml` and fill out the information with template 
```
---
[windows]
ansible_user: <ansibleAdministratorUser>
ansible_password: <ansibleAdministratorPassword>
ansible_connection: winrm
ansible_winrm_transport: <winrmauthenticationtransport>
ansible_winrm_server_cert_validation: <ignore|validate>
```

below is the sample of the `credentials.yml` files
```
---
[windows]
ansible_user: ansibleadmin
ansible_password: somestrongpassword
ansible_connection: winrm
ansible_winrm_transport: ntlm
ansible_winrm_server_cert_validation: ignore

```

To start the playbook simply run this command

```
 ansible-playbook -i hosts.ini -e '@credentials.yml' playbook.yml
```

Wait until playbook finished to run and see the result